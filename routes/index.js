/**
 *      main route
 */

const DashboardController   = require('../controllers/dashboard-controller');
const UserController        = require('../controllers/user-controller');
const AuthService           = require('../services/auth-service');


function registerRoutes(app) {
    app.get('/', DashboardController.bIndex);
    app.get('/platform', DashboardController.platform);
    app.get('/shop-perks', function(req, res) {
        res.redirect(process.env.WP_URL+'/perkstore');
    });
    app.get('/pricing', DashboardController.index);
    app.get('/trial', DashboardController.trial);
    app.get('/get-started', UserController.getStarted);
    app.get('/authentication', UserController.authentication);
    app.get('/thanks', UserController.thanks);
    app.get('/login', UserController.login);
    app.get('/hr-login', UserController.hrLogin);
    app.get('/employee-login', UserController.employeeLogin);
    app.get('/employee-login-trial', UserController.employeeLoginTrial);
    app.get('/sign', UserController.sign);
    app.get('/logout', UserController.logout);
    app.get('/v-a', DashboardController.index);
    app.post('/login', UserController.loginPost);
    app.post('/sign', UserController.signPost);
    app.post('/get-started', UserController.getStartedPost);
}

module.exports = registerRoutes;