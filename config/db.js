const mongoose = require('mongoose');

/**
 * Role: Connection with the database.
 * package: mongoose
 * 
 */

function connectToDB() {
    mongoose.connect(process.env.DB_URL, {
        useUnifiedTopology: true,
        useCreateIndex: true,
        useNewUrlParser: true,
        useFindAndModify: false
    }, error => {
        if (error) {
            console.log('Unable to connect to database');
            throw error;
        } else {
            console.log('Connected to MongoDB!');
        }
    });
}


module.exports = connectToDB;