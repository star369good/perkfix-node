const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const session = require('express-session');
const MemoryStore = require('memorystore')(session);
const cookieParser = require('cookie-parser');
const ejs = require('ejs');
const dotenv = require('dotenv');

dotenv.config();


/**
 * Determines the current environment and sets the appropriate variables
 * @param {Express App} app 
 */
function setEnvironment(app) {
    setDefaultEnv(app);
    if (isDevMode()) {
        setDevEnv(app);
    } else {
        setProdEnv(app);
    }
}

/**
 * check if it's development mode or not
 */
function isDevMode() {
    return (!process.env.NODE_ENV || process.env.NODE_ENV.toString().trim() !== 'production');
}

/**
 * Used to set default environment variables
 * @param {Express App} app 
 */
function setDefaultEnv(app) {
    process.env.PORT = process.env.PORT || 3000;
    process.env.DIST_PATH = __dirname + '/../dist';
    process.env.APP_NAME = process.env.APP_NAME || 'Perkfix';
    process.env.IS_HTTPS = (process.env.HTTP_HTTPS == "https");
    process.env.HOST = process.env.HOST || ((process.env.IS_HTTPS == "true")?'https://localhost':'http://localhost');
    process.env.BASE_URL = process.env.HOST + ":" + process.env.PORT; // base url of this api server
    process.env.PUBLIC_URL = process.env.PUBLIC_URL || process.env.BASE_URL;
    process.env.START_DATE = new Date(2020, 0, 1); // start date of this site
    app.use(cors());
    app.use(bodyParser.json()); // Allows parsing JSON from the client
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(express.static(process.env.DIST_PATH)); // dist path
    app.use(fileUpload({ createParentPath: true })); // enable files upload
    app.use(cookieParser());
    app.set('view engine', 'ejs');
}

/**
 * Used to set development environment variables
 * @param {Express App} app 
 */
function setDevEnv(app) {
    process.env.NODE_ENV = 'development';
    process.env.DB_URL = 'mongodb://localhost:27017/perkfix-development';
    process.env.TOKEN_SECRET = process.env.TOKEN_SECRET || 'be8xod82a9a66x9v6c9cfe95a62nqr0';
    process.env.API_NAME = process.env.API_NAME || 'TEST';
    app.use(morgan('dev')); // Log HTTP Requests to the node console (for debugging purposes)
    app.use(session({
        secret: process.env.TOKEN_SECRET,
        resave: true,
        saveUninitialized: true
    }));
}

/**
 * Used to set production environment variables
 * @param {Express App} app 
 */
function setProdEnv(app) {
    process.env.NODE_ENV = 'production';
    process.env.DB_URL = 'mongodb://localhost:27017/perkfix-production';
    process.env.TOKEN_SECRET = process.env.TOKEN_SECRET || 'zn47bd8bfa6x0rp3a9ec7g0wc5105wm5c';
    process.env.API_NAME = process.env.API_NAME || 'PRODUCT';
    app.use(session({
        cookie: { maxAge: 86400000 },
        store: new MemoryStore({
            checkPeriod: 86400000 // prune expired entries every 24h
        }),
        secret: process.env.TOKEN_SECRET,
        resave: true,
        saveUninitialized: true
    }));
}


module.exports = { setEnvironment, isDevMode };
