/**
 *      dashbaord controller
 */

const ThemeService = require('../services/theme-service');


const DashboardController = {

    /**
     *      GET /
     */
    index(req, res){
        res.render('pages/dashboard1', {
            ...ThemeService.getParams(),
            bottomTitle : "Home Page",
        });
    },
    /**
     *      GET /trial
     */
    trial(req, res){
        res.render('pages/trial', {
            ...ThemeService.getParams(),
            bottomTitle : "Start Trial",
        });
    },
    /**
     *      GET /platform
     */
    platform(req, res){
        res.render('pages/platform', {
            ...ThemeService.getParams(),
            bottomTitle : "Platform",
        });
    },
    /**
     *      GET /v-b
     */
    bIndex(req, res){
        res.render('version-b/dashboard1', {
            ...ThemeService.getParams(),
            bottomTitle : "Home Page",
        });
    },
};


module.exports = DashboardController;