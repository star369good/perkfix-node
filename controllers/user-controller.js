/**
 *      user controller
 */

const bcrypt = require('bcrypt-nodejs');
const UserModel = require('../models/user-model');
const EmployeeModel = require('../models/employee-model');
const HrEmployeeModel = require('../models/hremployee-model');
const AuthService = require('../services/auth-service');
const ThemeService = require('../services/theme-service');


const UserController = {

    /**
     *      get all informations of user
     */
    getUsers(userData, success, error, forceMultiple=false){

        // get all info from selected user
        
        try {
            UserModel.find(userData)
            .populate('employee')
            .populate('hremployee')
            .exec((err, users) => {
                if(err){
                    error(err);
                }
                else if (users != null) {
                    if (users.length > 1) success(users);
                    else if (users.length === 1) {  
                        if (forceMultiple) success(users);
                        else {
                            // get single user
                            let user = users[0];
                            user.api_token = AuthService.generateJWT(user);
                            success(user);
                        }
                    }
                    else error("There is nobody what you selected.");
                }
                else {
                    error("There is nobody what you selected.");
                }
            });
        }
        catch(e) {
            error(e);
        }

    },
    /**
     *      create new user
     *      create new poster, tasker, badge
     */
    createUser(userData, success, error){

        // create new user

        try {
            let user = new UserModel(userData);
            user.save((err, user) => {
                if(err){
                    error(err);
                }
                else {

                    // create new employee
                    let employee = new EmployeeModel({ user: user });
                    employee.save((err, employee) => {
                        if (err) {
                            error(err);
                        }
                        else {
                            user.employee = employee;

                            // success
                            success(user);
                        }
                    });
                }
            });
        }
        catch(e) {
            error(e);
        }

    },
    /**
     *      update user
     */
    updateUser(user, userData, success, error) {

        // update user

        try {
            // hash password
            if (userData.password) {
                userData.password = bcrypt.hashSync(userData.password);
            }
            let userId = user.id;
            UserModel.findOneAndUpdate({
                _id: userId
            }, {
                $set: userData
            }, {
                new:true
            }, (err, user) => {
                if(err){
                    error(err);
                }
                else {
                    success(user);
                }
            });
        }
        catch(e) {
            error(e);
        }
        
    },
    /**
     *      delete users
     */
    deleteUsers(userIds, success, error) {

        // delete users

        try {
            UserModel.remove({
                _id : {
                    $in : userIds
                }
            }, (err, result) => {
                if(err){
                    error(err);
                }
                else {

                    // delete employee
                    EmployeeModel.remove({
                        user : {
                            $in : userIds
                        }
                    }, (err, data) => {
                        if(err){
                            error(err);
                        }
                        else {
                            
                            // success
                            success(result);
                        }
                    });
                }
            });
        }
        catch(e) {
            error(e);
        }

    },
    /**
     *      GET /login
     */
    login(req, res) {
        res.render('pages/login', {
            ...ThemeService.getParams(),
            bottomTitle : "Log In",
        });
    },
    /**
     *      GET /hr-login
     */
    hrLogin(req, res) {
        res.render('pages/hr-login', {
            ...ThemeService.getParams(),
            bottomTitle : "Log In for HR employee",
        });
    },
    /**
     *      GET /employee-login
     */
    employeeLogin(req, res) {
        res.render('pages/employee-login', {
            ...ThemeService.getParams(),
            bottomTitle : "Log In for Employee",
        });
    },
    /**
     *      GET /employee-login-trial
     */
    employeeLoginTrial(req, res) {
        res.render('pages/employee-login-trial', {
            ...ThemeService.getParams(),
            bottomTitle : "Log In for Employee",
        });
    },
    /**
     *      log in - redirect
     */     
    loginPost(req, res){
        // console.log(req.body);

        UserController.getUsers({
            email : req.body.email
        }, function(data) {
            // success
            if (data != null && data._id) {

                // check password matching
                if (!UserModel.passwordMatches(req.body.password, data.password)) {
                    console.log("Password is wrong!");
                    res.redirect('/user/login');
                }
                else {
                    // console.log(data);
                    req.session.authorization = data['api_token'];
                    res.redirect('/');
                }
            }
        }, function(err) {
            // error
            console.log(err);
            res.redirect('/user/login');
        });
    },
    /**
     *      sign up
     *      render sign.ejs
     */
    sign(req, res) {
        // req.session.destroy(function(){
        //     console.log("clear session.");
        // });
        res.render('pages/sign', {
            ...ThemeService.getParams(),
            bottomTitle : "Sign Up",
        });
    },
    /**
     *      sign up - redirect
     */     
    signPost(req, res){
        // console.log(req.body);
        
        UserController.createUser({
            email : req.body.email,
            password : req.body.password
        }, function(data){
            // success
            req.session.authorization = data['api_token'];
            res.redirect('/');
        }, function(err){
            // error
            console.log(err);
            res.redirect('/user/sign');
        });
    },
    /**
     *      log out - redirect
     *      session destroy
     */     
    logout(req, res){
        req.session.destroy(function(){
            console.log("user logged out.");
        });
        res.redirect('/');
    },
    /**
     *      GET /get-started
     */     
    getStarted(req, res){
        res.render('pages/get-started', {
            ...ThemeService.getParams(),
            bottomTitle : "Get Started",
        });
    },
    /**
     *      POST /get-started
     */     
    getStartedPost(req, res){
        res.redirect('/authentication');
    },
    /**
     *      GET /authentication
     */     
    authentication(req, res){
        res.render('pages/authentication', {
            ...ThemeService.getParams(),
            bottomTitle : "Authentication",
        });
    },
    /**
     *      GET /thanks
     */     
    thanks(req, res){
        res.render('pages/thanks', {
            ...ThemeService.getParams(),
            bottomTitle : "Thanks",
        });
    },
};


module.exports = UserController;