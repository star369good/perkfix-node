/**
 *      user model
 */


const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const jwt = require('jsonwebtoken');

var UserSchema = new mongoose.Schema({
    email : {
        type: String,
        unique : true,
        required : true
    },
    first_name : {
        type : String,
        default : null
    },
    last_name : {
        type : String,
        default : null
    },
    password : {
        type : String,
        default : null
    },
    api_token : {
        type : String,
        default : null
    },
    contact_number : {
        type : Number,
        default : null
    },
    employee : {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'employee',
        default : null
    },
    hr_employee : {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'hremployee',
        default : null
    },
    is_hr_employee : {
        type : Boolean,
        default : false
    },
    status : {
        type : String,
        enum : ['Active', 'Locked'],
        default : 'Active'
    },
    card_number : {
        type : Number,
        default : null
    },
    card_type : {
        type : String,
        enum : ['Credit', 'Debit']
    },
    routing_number : {
        type : String,
        default : null
    },
    bank_kind : {
        type : String,
        default : null
    },
    bank_email : {
        type : String,
        default : null
    },
    bank_password : {
        type : String,
        default : null
    },
    created_at : {
        type : Date,
        default : Date.now
    },
});

UserSchema.statics.passwordMatches = function(password, hash){
    return bcrypt.compareSync(password, hash);
};
UserSchema.pre('save', function(next){
    if (!this.api_token) {
        const unsafePassword = this.password;
        this.password = bcrypt.hashSync(unsafePassword);
        this.api_token = jwt.sign({ email : this.email }, process.env.TOKEN_SECRET);
    }
    next();
});
UserSchema.virtual('slug').get(function() {
    return encodeURI([this.first_name, this.last_name].join('-').toLowerCase()) + '-' + this._id;
});
UserSchema.virtual('type').get(function() {
    return (this.is_hr_employee) ? 'Approver' : 'Basic';
});


var UserModel = mongoose.model('user', UserSchema);

module.exports = UserModel;