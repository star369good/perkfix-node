/**
 *      transaction model
 */


const mongoose = require('mongoose');

var TransactionSchema = new mongoose.Schema({
    name : {
        type : String,
        default : null
    },
    amount : {
        type : Number,
        default : null
    },
    year_month : {
        type : String,
        default : null
    },
    from : {
        type : String,
        default : null
    },
    to : {
        type : String,
        default : null
    },
    perk : {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'perk',
        default: null
    },
    created_at : {
        type : Date,
        default : Date.now
    },
});


var TransactionModel = mongoose.model('transaction', TransactionSchema);

module.exports = TransactionModel;