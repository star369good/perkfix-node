/**
 *      organization model
 */


const mongoose = require('mongoose');

var OrganizationSchema = new mongoose.Schema({
    entity_name : {
        type : String,
        default : null
    },
    primary : {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'hremployee'
    },
    secondary : {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'hremployee'
    },
    teams : [{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'team'
    }],
    transactions : [{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'transaction'
    }],
    balance : {
        type : Number,
        default : null
    },
    current_balance : {
        type : String,
        default : null
    },
    entity_tye : {
        type : String,
        enum : ['S-Corp'],
        default : null
    },
    website : {
        type : String,
        default : null
    },
    duns_number : {
        type : String,
        default : null
    },
    ein_tax_id : {
        type : String,
        default : null
    },
    street : {
        type : String,
        default : null
    },
    city : {
        type : String,
        default : null
    },
    state : {
        type : String,
        default : null
    },
    country : {
        type : String,
        default : null
    },
    zip : {
        type : String,
        default : null
    },
    unit : {
        type : String,
        default : null
    },
    created_at : {
        type : Date,
        default : Date.now
    },
});


var OrganizationModel = mongoose.model('organization', OrganizationSchema);

module.exports = OrganizationModel;