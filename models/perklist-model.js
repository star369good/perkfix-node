/**
 *      perk list model
 */


const mongoose = require('mongoose');

var PerklistSchema = new mongoose.Schema({
    name : {
        type : String,
        default : null
    },
    amount : {
        type : Number,
        default : null
    },
    year_month : {
        type : String,
        default : null
    },
    from : {
        type : String,
        default : null
    },
    to : {
        type : String,
        default : null
    },
    perk : {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'perk',
        default: null
    },
    transaction : {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'transaction',
        default: null
    },
    created_at : {
        type : Date,
        default : Date.now
    },
});


var PerklistModel = mongoose.model('perklist', PerklistSchema);

module.exports = PerklistModel;