/**
 *      team model
 */


const mongoose = require('mongoose');

var TeamSchema = new mongoose.Schema({
    name : {
        type : String,
        default : null
    },
    employees : [{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'employee'
    }],
    organization : {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'organization'
    },
    created_at : {
        type : Date,
        default : Date.now
    },
});


var TeamModel = mongoose.model('team', TeamSchema);

module.exports = TeamModel;