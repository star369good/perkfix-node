/**
 *      HR employee model
 */


const mongoose = require('mongoose');

var HrEmployeeSchema = new mongoose.Schema({
    user : {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'user'
    },
    organization : {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'organization'
    },
    perklists : [{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'perklist'
    }],
    created_at : {
        type : Date,
        default : Date.now
    },
});


var HrEmployeeModel = mongoose.model('hremployee', HrEmployeeSchema);

module.exports = HrEmployeeModel;