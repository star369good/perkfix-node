/**
 *      employee model
 */


const mongoose = require('mongoose');

var EmployeeSchema = new mongoose.Schema({
    user : {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'user'
    },
    team : {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'team'
    },
    perklists : [{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'perklist'
    }],
    transactions : [{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'transaction'
    }],
    budget : {
        type : Number,
        default : 0
    },
    balance : {
        type : Number,
        default : 0
    },
    created_at : {
        type : Date,
        default : Date.now
    },
});


var EmployeeModel = mongoose.model('employee', EmployeeSchema);

module.exports = EmployeeModel;