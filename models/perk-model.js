/**
 *      perk model
 */


const mongoose = require('mongoose');

var PerkSchema = new mongoose.Schema({
    title : {
        type : String,
        default : null
    },
    button_link : {
        type : String,
        default : null
    },
    button_text : {
        type : String,
        default : null
    },
    compatible_devices : {
        type : String,
        default : null
    },
    currency_symbol : {
        type : String,
        default : null
    },
    note : {
        type : String,
        default : null
    },
    perk_detail : {
        type : String,
        default : null
    },
    perk_name : {
        type : String,
        default : null
    },
    price : {
        type : String,
        default : null
    },
    cosmetic_images : [{
        type : String,
        default : null
    }],
    provider_website_link : {
        type : String,
        default : null
    },
    compatible_devices_link : {
        type : String,
        default : null
    },
    helpful_information_titles : [{
        type : String,
        default : null
    }],
    helpful_information_links : [{
        type : String,
        default : null
    }],
    price_type : {
        type : String,
        enum : ['yr', 'mo'],
        default : 'yr'
    },
    created_at : {
        type : Date,
        default : Date.now
    },
});


var PerkModel = mongoose.model('perk', PerkSchema);

module.exports = PerkModel;