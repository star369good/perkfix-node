/**
 *      main theme view service
 */



const ThemeService = {
    /**
     *      get default parameters
     */
    getParams() {
        return {
            baseURL : process.env.BASE_URL,
            publicURL : process.env.PUBLIC_URL,
            title : process.env.APP_NAME,
            bottomTitle : 'Home Page',
            isShowError : false,
            errorMessage : '',
        };
    },
};


module.exports = ThemeService;