/**
 * Role: Handling all authentication
 * Package: jsonwebtoken
 */


const jwt = require('jsonwebtoken');

const AuthService = {
    /**
     * Role: generating the token for identify the users.
     * Param: user model.
     */
    generateJWT(user){
        const tokenData = {
            id : user._id,
            email : user.email,
            api_token : user.api_token
        };
        return jwt.sign({ user : tokenData }, process.env.TOKEN_SECRET);
    },
    /**
     * Role: get the token
    */
    getToken(req){
        const token = req.headers.authorization || req.headers['authorization'] || req.session.authorization;
        return token;
    },
    /**
     * Role: decode from token
    */
    decodeFromToken(token){
        if(!token){
            return null;
        }
    
        try{
            return jwt.verify(token, process.env.TOKEN_SECRET);
        }
        catch(error){
            return null;
        }
    },
    /**
     * Role: decode the token
    */
    decodeToken(req){
        const token = AuthService.getToken(req);
        return AuthService.decodeFromToken(token);
    },
    /**
     * Role: If there is no token, can be guess this user is not verified, so we can ask the login.
     * 
     */
    requireLogin(req, res, next){
        const token = AuthService.decodeToken(req);
        if(!token){
            res.status(401).json();
        }
        else {
            next();
        }
    },
    /**
     * Role: If there is no loggined, can be guess this user is not verified, so we redirect /login.
     * 
     */
    requireLoginRedirectLogin(req, res, next){
        const token = AuthService.decodeToken(req);
        if(!token){
            res.redirect('/user/login');
        }
        else {
            next();
        }
    },
    /**
     * Role: check if loggined or not, run callbacks
     * 
     */
    requireLoginRedirectCallbacks(req, res, next, back){
        const token = AuthService.decodeToken(req);
        if(!token){
            back();
        }
        else {
            next();
        }
    },
    /**
     * 
     * Role: getting the userid from the parameter
     */
    getUserId(req){
        const token = AuthService.decodeToken(req);
        if(!token){
            return null;
        }
        return token.user.id;
    },
    /**
     * 
     * Role: getting the email from the parameter
     */
    getUserEmail(req){
        const token = AuthService.decodeToken(req);
        if(!token){
            return null;
        }
        return token.user.email;
    },
    /**
     * 
     * Role: getting the api_token from the parameter
     */
    getUserToken(req){
        const token = AuthService.decodeToken(req);
        if(!token){
            return null;
        }
        return token.user.api_token;
    }
};


module.exports = AuthService;