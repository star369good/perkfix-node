const express = require('express');
const http = require('http');
const https = require('https');
const Environment = require('../config/env');
const connectToDB = require('../config/db');
const registerRoutes = require('../routes');
const fs = require('fs');


const app = express();


// environment
Environment.setEnvironment(app);

var server;
if (process.env.IS_HTTPS == "true") {
    // https server
    const options = {
        key: fs.readFileSync(process.env.KEY_PATH),
        cert: fs.readFileSync(process.env.CERT_PATH),
    };
    if (typeof process.env.CA_PATH !== "undefined" && process.env.CA_PATH != "") options.ca = fs.readFileSync(process.env.CA_PATH);

    server = https.createServer(options, app);
    console.log("https server");
}
else {
    server = http.Server(app);
    console.log("http server");
}


// mongo db
connectToDB();

// routes
registerRoutes(app);


// All non-API requests made to the server, for example, http://www.homepage.com/,
// will hit this request, which just returns the main layout, html file
app.get('*', (req, res) => {
    if (Environment.isDevMode()) {
        return res.send('Running server in development mode.');
    } else {
        // Returns the main index file in production environment
        return res.sendFile('maintenance.html', { root: process.env.DIST_PATH });
    }
});

// Starts the server on the given port
server.listen(process.env.PORT, () => {
    console.log('MEVN app listening on port ' + process.env.PORT + ' in ' + process.env.NODE_ENV + ' mode!');
});