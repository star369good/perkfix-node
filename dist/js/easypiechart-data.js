/*Easypiechart Init*/

$(document).ready(function() {
	"use strict";
	if( $('#pie_chart_1').length > 0 ){
		$('#pie_chart_1').easyPieChart({
			barColor : '#f0166c',
			lineWidth: 15,
			animate: 1000,
			size:	150,
			lineCap: 'round',
			scaleColor: '#000',
			trackColor: '#51102d',
			onStep: function(from, to, percent) {
				$(this.el).find('.percent').text('3.9');
			}
		});
	}
});